'use strict';

// Paquetes npm de node
const argv = require('yargs').argv;

const gulp = require('gulp'),
      del  = require('del'),
      dest = require('gulp-dest'),
      gulpCopy = require('gulp-copy'),
      gulpImagemin = require('gulp-imagemin'),
      gulpSass = require('gulp-sass'),
      sourcemaps = require('gulp-sourcemaps'),
      gulpJSLint = require('gulp-jshint'),
      jshintStylish = require('jshint-stylish'),
      concat = require('gulp-concat'),
      uglify = require('gulp-uglify'),
      browerSync = require('browser-sync').create();

// Paths de ubicación del código fuente
var srcPaths = {
  data: 'src/data/',
  img: 'src/img/',
  js: 'src/js/',
  scss: 'src/scss/',
  vendor: 'src/vendor/',
  files: 'src/'
};

// Path de ubicación del código de distribución o producción
var distPaths = {
  data: 'dist/data/',
  img: 'dist/img/',
  js: 'dist/js/',
  css: 'dist/css/',
  vendor: 'dist/vendor/',
  files: 'dist/'
};

// Gulp task clean ==> borra dist/*
gulp.task('clean', ()=>
{
  del([`${distPaths.data}**/*`,`${distPaths.files}*.html`,`${distPaths.img}**/*`,`${distPaths.js}*.js`,`${distPaths.css}*`,`${distPaths.vendor}**/*`]).then(paths =>{
    console.log(`Following files and folders have been deleted:${paths.join('\n')}`);
  });
});

// Gulp task html ==> copia el archivo src/index.html
gulp.task('html', () =>
{
   return gulp.src([`${srcPaths.files}index.html`])
              .pipe(gulp.dest(`${distPaths.files}`))
              .pipe(browerSync.stream());
});

// Gulp task copy
gulp.task('copy', ['html'], () =>
{
  console.log("[Gulp Task] copy");
  return gulp.src([`${srcPaths.vendor}**/*`, `${srcPaths.data}**/*`])
             .pipe(gulpCopy(`${distPaths.files}`, { prefix: 1 }))
             .pipe(browerSync.stream());
});

// Gulp task imagemin ==> Comprime las imágenes
gulp.task('imagemin', () =>
{
  console.log("[Gulp Task] imagemin");
  return gulp.src([`${srcPaths.img}**/*`])
             .pipe(gulpImagemin())
             .pipe(sourcemaps.write())
             .pipe(gulp.dest(`${distPaths.img}`))
             .pipe(browerSync.stream());
});

// Gulp task scss ==> Preprocesamiento CSS [Sass]
gulp.task('scss', () =>
{
  console.log("[Gulp Task] scss");
  return gulp.src(`${srcPaths.scss}base.scss`)
      .pipe(sourcemaps.init())
      .pipe(gulpSass())
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(`${distPaths.css}`))
      .pipe(browerSync.stream());
});

// Gulp task lint ==> Comprobación errores del código Javascript
gulp.task('lint', () =>
{
    console.log("[Gulp Task] lint");
    return gulp.src([`${srcPaths.js}*.js`])
               .pipe(gulpJSLint())
               .pipe(gulpJSLint.reporter(jshintStylish));
});

// Gulp task js ==> Ejecución de la tarea lint y tratamiento del código Javascript (concatenar y minificar)
gulp.task('js', ['lint'], () =>
{
    console.log("[Gulp Task] js");
    // Genero el fichero all.min.js si me devuelve errores el callback?
    return gulp.src([`${srcPaths.js}contact.js`,`${srcPaths.js}contactlist.js`, `${srcPaths.js}main.js`])
      .pipe(concat('all.min.js'))
      .pipe(gulp.dest(`${distPaths.js}`))
      .pipe(uglify())
      .pipe(gulp.dest(distPaths.js))
      .pipe(browerSync.stream());
});

// Gulp task serve ==> Build completo del proyecto
gulp.task('serve', ['copy','imagemin','scss', 'js'],() =>
{
    console.log("[Gulp Task] serve");

    let options = {
      logLevel: "info",
      browser: ["chrome"]
    };

    if(argv.XAMPP){
      options.proxy = argv.proxy || "localhost";
      options.startPath = "/contacts-agenda/dist";
      options.port = 8000;
    }else{
      options.server = { baseDir: distPaths.files };
    }

    if(argv.browser != null){
      options.browser.push(argv.browser);
    }
    console.log("[Options]:"+JSON.stringify(options));
    browerSync.init(options);

    // Observando cambios...
    gulp.watch(`${srcPaths.files}*.html`, ['html']);
    gulp.watch(`${srcPaths.img}**/*`, ['imagemin']);
    gulp.watch(`${srcPaths.scss}**/*`, ['scss']);
    gulp.watch(`${srcPaths.js}*.js`, ['js']);
});

gulp.task('default', ['clean', 'serve'], () =>
{
  console.log("[Gulp Task] default ");
});
