ContactList = function () {

    this.$contactForm = $(ContactList.DOM_CONTACT_INFO);
    this.$newContact = $(ContactList.DOM_NEW_BUTTON);
    this.$saveContact = $(ContactList.DOM_SAVE_BUTTON);
    this.$deleteContact = $(ContactList.DOM_DELETE_BUTTON);
    this.$contactList = $(ContactList.DOM_CONTACT_LIST);
    this.$search = $(ContactList.DOM_SEARCH);

    this._idCounter = 0;
    this._contactList = [];
    this._editedContactID = ContactList.NEW_CONTACT_ID;

    this.init();
};

ContactList.DOM_CONTACT_LIST_CONTAINER = '#contact-list-container';
ContactList.DOM_CONTACT_LIST = '#contact-list';
ContactList.DOM_CONTACT_LIST_NO_CONTACTS = '#no-contacts';
ContactList.DOM_SEARCH = '#search';
ContactList.DOM_ALERT_FIRST_TIME = '#alert-first-time';
ContactList.DOM_CONTACT_INFO = '#contact-info';
ContactList.DOM_CONTACT_ID = '#contact-id';
ContactList.DOM_CONTACT_FIRSTNAME = '#contact-firstname';
ContactList.DOM_CONTACT_LASTNAME = '#contact-lastname';
ContactList.DOM_CONTACT_PHONENUMBER = '#contact-phonenumber';
ContactList.DOM_NEW_BUTTON = '#new-contact';
ContactList.DOM_SAVE_BUTTON = '#save-contact';
ContactList.DOM_DELETE_BUTTON = '#delete-contact';


ContactList.PERSISTENT_OBJECT_KEY = 'contactlist';
ContactList.PERSISTENT_OBJECT_DATA_PATH = 'data/defaultdata.json';

ContactList.PERSISTENT_OBJECT_CONTACT_FIRSTNAME = 'nombre';
ContactList.PERSISTENT_OBJECT_CONTACT_LASTNAME = 'apellidos';
ContactList.PERSISTENT_OBJECT_CONTACT_PHONENUMBER = 'telefono';

ContactList.NEW_CONTACT_ID = -1;

ContactList.prototype.init = function () {
    var _self = this;

    this.$contactList.on('click', 'a', function (e) { _self.onContactSelected(e); });
    this.$search.on('input', function (e) { _self.onSearch(e); });
    this.$newContact.on('click', function (e) { _self.setupNewContact(e); });
    this.$saveContact.on('click', function (e) { _self.saveContact(e); });
    this.$deleteContact.on('click', function (e) { _self.deleteContact(e); });

    this.checkContactListData();
};

ContactList.prototype.checkContactListData = function() {
    var storedData;
    var $alertFirstTime;

    storedData = store.get(ContactList.PERSISTENT_OBJECT_KEY);
    $alertFirstTime = $(ContactList.DOM_ALERT_FIRST_TIME);

    if ( storedData ) {
        $alertFirstTime.remove();
        this.onLoadContactsDataSuccess( storedData );
    }
    else {
        this.loadFirstTimeData();
    }
};

ContactList.prototype.loadFirstTimeData = function() {
    var _self = this;

    $.ajax({
        dataType: "json",
        url: ContactList.PERSISTENT_OBJECT_DATA_PATH,
        success: function(data) { _self.onLoadContactsDataSuccess(data); },
        error: function() { _self.onLoadContactsDataError(); }
    });
};

ContactList.prototype.onLoadContactsDataSuccess = function( data ) {
    var i;
    var nContacts;
    var currentContact;

    nContacts = data.length;
    for (i=0; i<nContacts; i++)
    {
    currentContact = new Contact(this._idCounter, data[i].nombre, data[i].apellidos, data[i].telefono, true);
    this.addContact( currentContact, false );
    }

    this.resetContactForm();
    this.render();
};

ContactList.prototype.onLoadContactsDataError = function() {
    console.log('error loading data.');
};

ContactList.prototype.savePersistentData = function() {
    var i;
    var nContacts;
    var currentContact;
    var savedContact;
    var contactListJSON;

    contactListJSON = [];
    nContacts = this._contactList.length;
    for (i=0; i<nContacts; i++)
    {
        currentContact = this._contactList[i];

        savedContact = {};
        savedContact[ContactList.PERSISTENT_OBJECT_CONTACT_FIRSTNAME] = currentContact.getFirstName();
        savedContact[ContactList.PERSISTENT_OBJECT_CONTACT_LASTNAME] = currentContact.getLastName();
        savedContact[ContactList.PERSISTENT_OBJECT_CONTACT_PHONENUMBER] = currentContact.getPhoneNumber();
        contactListJSON.push( savedContact );
    }

    store.clear();
    store.set( ContactList.PERSISTENT_OBJECT_KEY, contactListJSON);
};

ContactList.prototype.resetContactForm = function() {
    this.$contactForm.css('display', 'none');

    this._editedContactID = ContactList.NEW_CONTACT_ID;
    this.$contactForm.find(ContactList.DOM_CONTACT_FIRSTNAME).val('');
    this.$contactForm.find(ContactList.DOM_CONTACT_LASTNAME).val('');
    this.$contactForm.find(ContactList.DOM_CONTACT_PHONENUMBER).val('');

    this.$saveContact.css('display', 'none');
    this.$deleteContact.css('display', 'none');
};

ContactList.prototype.searchContactById = function( id ) {
    var found = false;
    var i = 0;
    var nContacts = this._contactList.length;
    var currentContact;

    while (i<nContacts && !found) {
        currentContact = this._contactList[i];

        if (currentContact.getId() == id) {
            found = true;
        }
        else {
            i++;
        }
    }

    return currentContact;
};

ContactList.prototype.filter = function( term ) {
    var i;
    var currentContact;
    var fullNameAndPhone;
    var searchRegExp = new RegExp( term , 'i');

    for (i=0; i<this._contactList.length; i++)
    {
        currentContact = this._contactList[i];
        fullNameAndPhone = currentContact.getFullSearhableInfo();
        if ( fullNameAndPhone.search(searchRegExp)<0 ) {
            currentContact.setVisible( false );
        } else {
            currentContact.setVisible( true );
        }
    }

    this.render();
};

ContactList.prototype.onContactSelected = function( e ) {
    var $target = $(e.currentTarget);
    var contactInfo;

    this._editedContactID = parseInt( $target.data('id') );
    contactInfo = this.searchContactById( this._editedContactID );

    this.$contactForm.find(ContactList.DOM_CONTACT_FIRSTNAME).val( contactInfo.getFirstName() );
    this.$contactForm.find(ContactList.DOM_CONTACT_LASTNAME).val( contactInfo.getLastName() );
    this.$contactForm.find(ContactList.DOM_CONTACT_PHONENUMBER).val( contactInfo.getPhoneNumber() );

    this.$contactForm.css('display', '');
    this.$saveContact.css('display', '');
    this.$deleteContact.css('display', '');
};

ContactList.prototype.onSearch = function( e ) {
    var searchTerm = this.$search.val();

    if (searchTerm !== '') {
        this.filter( searchTerm );
    }
    else {
        this.render( true );
    }
};

ContactList.prototype.saveContact = function( e ) {
    var contactFirstname;
    var contactLastname;
    var contactPhonenumber;
    var contactInfo;

    contactFirstname = this.$contactForm.find(ContactList.DOM_CONTACT_FIRSTNAME).val();
    contactLastname = this.$contactForm.find(ContactList.DOM_CONTACT_LASTNAME).val();
    contactPhonenumber = this.$contactForm.find(ContactList.DOM_CONTACT_PHONENUMBER).val();

    if ( this._editedContactID === ContactList.NEW_CONTACT_ID ) {
        contactInfo = new Contact(this._idCounter, contactFirstname, contactLastname, contactPhonenumber, true);
        this.addContact( contactInfo, true );
    }
    else {
        contactInfo = new Contact(null, contactFirstname, contactLastname, contactPhonenumber, true);
        this.editContact( this._editedContactID, contactInfo );
    }

    this.resetContactForm();
};

ContactList.prototype.deleteContact = function() {
    this.deleteEditedContact( this._editedContactID );
    this.resetContactForm();
};

ContactList.prototype.setupNewContact = function( e ) {
    this.$contactForm.css('display', 'inherit');

    this._editedContactID = ContactList.NEW_CONTACT_ID;
    this.$contactForm.find(ContactList.DOM_CONTACT_FIRSTNAME).val('');
    this.$contactForm.find(ContactList.DOM_CONTACT_LASTNAME).val('');
    this.$contactForm.find(ContactList.DOM_CONTACT_PHONENUMBER).val('');

    this.$saveContact.css('display', '');
    this.$deleteContact.css('display', 'none');
};


ContactList.prototype.addContact = function( newContact, forceRender ) {
    this._idCounter++;
    this._contactList.push( newContact );

    this.savePersistentData();
    if (forceRender) { this.render(); }
};

ContactList.prototype.editContact = function( id, newContactData ) {
    var editedContact = this.searchContactById(id);

    editedContact.update( newContactData );
    this.savePersistentData();
    this.render();
};

ContactList.prototype.deleteEditedContact = function( id ) {
    var found = false;
    var i = 0;
    var nContacts = this._contactList.length;
    var currentContact;

    while (i<nContacts && !found) {
        currentContact = this._contactList[i];

        if (currentContact.getId() == id) {
            found = true;
            this._contactList.splice(i, 1);
        }
        else {
            i++;
        }
    }

    this.savePersistentData();
    this.render();
};

ContactList.prototype.render = function(all) {
    var i;
    var currentContact;
    var $contactListContainer = $(ContactList.DOM_CONTACT_LIST_CONTAINER);
    var $contactList = $(ContactList.DOM_CONTACT_LIST);
    var $noContacts = $(ContactList.DOM_CONTACT_LIST_NO_CONTACTS);
    var visibleContacts = 0;

    this.$contactList.detach();
    this.$contactList.empty();

    if (all === undefined) { all = false; }

    for (i=0; i<this._contactList.length; i++)
    {
        currentContact = this._contactList[i];
        if (all) { currentContact.setVisible(true); }

        if (currentContact.getVisible()) {
            visibleContacts++;
            this.$contactList.append( currentContact.getHTML() );
        }
    }

    if (!visibleContacts) {
        $noContacts.show();
        $contactList.hide();
    }
    else {
        $noContacts.hide();
        $contactList.show();
    }

    $contactListContainer.append( this.$contactList );
};
