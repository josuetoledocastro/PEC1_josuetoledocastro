Contact = function (id, firstName, lastName, phoneNumber, visibility) {
    this._id = id;
    this._firstName = firstName;
    this._lastName = lastName;
    this._phoneNumber = phoneNumber;
    this._visible = visibility;
};

Contact.ITEM_TEMPLATE =
    '<li class="list-group-item">' +
      '<a href="#" data-id="{{ID}}" class="list-group-item">' +
        '<h2 class="list-group-item-fullName">{{FULLNAME}}</h2>' +
        '<p class="list-group-item-phone">{{PHONENUMBER}}</p>' +
      '</a>' +
    '</li>';
Contact.ID_REGEXP = /{{ID}}/gi;
Contact.FULLNAME_REGEXP = /{{FULLNAME}}/gi;
Contact.PHONENUMBER_REGEXP = /{{PHONENUMBER}}/gi;


Contact.prototype.getId = function() {
    return this._id;
};

Contact.prototype.getFirstName = function() {
    return this._firstName;
};

Contact.prototype.setFirstName = function( firstName ) {
    this._firstName = firstName;
};

Contact.prototype.getLastName = function() {
    return this._lastName;
};

Contact.prototype.setLastName = function( lastName ) {
    this._lastName = lastName;
};

Contact.prototype.getPhoneNumber = function() {
    return this._phoneNumber;
};

Contact.prototype.setPhoneNumber = function( phoneNumber ) {
    this._phoneNumber = phoneNumber;
};

Contact.prototype.getVisible = function() {
    return this._visible;
};

Contact.prototype.setVisible = function( visibility ) {
    this._visible = visibility;
};

Contact.prototype.update = function( contactInfo ) {
    this._firstName = contactInfo.getFirstName();
    this._lastName = contactInfo.getLastName();
    this._phoneNumber = contactInfo.getPhoneNumber();
    this._visible = contactInfo.getVisible();
};

Contact.prototype.getFullSearhableInfo = function() {
    return this._lastName + ' ' + this._firstName + ' ' + this._phoneNumber;
};

Contact.prototype.getHTML = function() {
    var currentContactHTML;

    currentContactHTML = Contact.ITEM_TEMPLATE;
    currentContactHTML = currentContactHTML.replace(Contact.ID_REGEXP, String(this._id));
    currentContactHTML = currentContactHTML.replace(Contact.FULLNAME_REGEXP, this._lastName+', '+this._firstName);
    currentContactHTML = currentContactHTML.replace(Contact.PHONENUMBER_REGEXP, this._phoneNumber);

    return currentContactHTML;
};
